#include <stdio.h>
#include <stdlib.h>
#include "def.h"
#include "fun.h"
#include "fun4.h"
#include "fun5.h"
#include "fun6.h"

int main()
{
    int opcija;
    
    do{
        //system("clear");
        printf("Ponudjene si vam sledeće funkcionalnosti:\n");
        printf("1.  Formiranje datoteke\n");
        printf("2.  Zadavanje naziva datoteke\n");
        printf("3.  Prikaz naziva aktivne datoteke\n");
        printf("4.  Formiranje serijske datoteke\n");
        printf("5.  Formiranje sekvencijalne datoteke\n");
        printf("6.  Formiranje aktivne datoteke \n");
        printf("7.  Upis novog sloga u aktivnu datoteku\n");
        printf("8.  Traženje proizvoljnog sloga\n");
        printf("9.  Logičko brisanje aktuelnog sloga iz aktivne datoteke\n");
        printf("10. Prikaz svih slogova, adresu bloka sloga i rednim brojem sloga u bloku\n");
        printf("11. Čuvanje sadržaja datoteke u .csv fajl\n");
        printf("12. Prikaz specijalnih slogova\n");
        printf("\n\n\tVaša opcija u celobrojnim vrednostima:\t");

        scanf("%d",&opcija);

        switch (opcija){
            case 1:
                formirajDatoteku();
                break;
            case 2:
                odabirAktivne();
                break;
            case 3:
                prikazAktivne();
                break;
            case 4:
                formirajSerijsku();
                break;
            case 5:
                formirajSekvencijalnu();
                break;
            case 6:
                formirajIndexSekvencijalnu();
                break;
            case 7:
                break;
            case 8:
                break;
            case 9:
                break;
            case 10:
                break;
            case 11:
                break;
            case 12:
                break;
            case 13:
                break;
            case 14:
                ispisiSveSlogove();
                break;
            default:
                system("clear");
                system("echo 'Izabrali ste nepostojecu opciju'");
                sleep(2);
            }
    }while(opcija != 13);
    
    return 0;
}

