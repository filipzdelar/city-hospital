#ifndef FUN4_H_INCLUDED
#define FUN4_H_INCLUDED


#include "def.h"

#include <unistd.h>
#include <string.h>

char aktivna[30];

void dodajSlog(SLOG * slog) {

    FILE* fajl = fopen(aktivna, "rb+");
	if (fajl == NULL) {
		printf("Datoteka nije otvorena!\n");
		return;
	}
    
    BLOK blok;
    // Provera da li je prvi prazan blok
    if(fread(&blok, sizeof(BLOK), 1, fajl))
    {
        printf("Nije prazna bila");
        BLOK blok;
        fseek(fajl, -sizeof(BLOK), SEEK_END); //u poslednji blok upisujemo novi slog
        fread(&blok, sizeof(BLOK), 1, fajl);

        int i;
        for (i = 0; i < 5; i++) {
            if (strcmp(blok.bolesnik[i].evidencioniBroj, OZNAKA_KRAJA_DATOTEKE) == 0) {
                //Ovo je mesto gde se nalazi slog sa oznakom
                //kraja datoteke; tu treba upisati novi slog.
                memcpy(&blok.bolesnik[i], slog, sizeof(SLOG));
                break;
            }
        }

        i++; //(na to mesto u bloku cemo upisati krajDatoteke)

        if (i < 5) {
            //Jos uvek ima mesta u ovom bloku, mozemo tu smestiti slog
            //sa oznakom kraja datoteke.
            strcpy(blok.bolesnik[i].evidencioniBroj, OZNAKA_KRAJA_DATOTEKE);

            //Sada blok mozemo vratiti u datoteku.
            fseek(fajl, -sizeof(BLOK), SEEK_CUR);
            fwrite(&blok, sizeof(BLOK), 1, fajl);
        } else {
            //Nema vise mesta u tom bloku, tako da moramo
            //praviti novi blok u koji cemo upisati slog
            //sa oznakom kraja datoteke.

            //Prvo ipak moramo vratiti u datoteku blok
            //koji smo upravo popunili:
            fseek(fajl, -sizeof(BLOK), SEEK_CUR);
            fwrite(&blok, sizeof(BLOK), 1, fajl);

            //Okej, sad pravimo novi blok:
            BLOK noviBlok;
            strcpy(noviBlok.bolesnik[0].evidencioniBroj, OZNAKA_KRAJA_DATOTEKE);

            //noviBlok.bolesnik[0].evidencioniBroj = OZNAKA_KRAJA_DATOTEKE;
            //(vec smo na kraju datoteke, nema potrebe za fseekom)
            fwrite(&noviBlok, sizeof(BLOK), 1, fajl);
        }
    }else
    {
        printf("Prazna bila");
        fseek(fajl, 0, SEEK_SET);
        fwrite(&slog, sizeof(SLOG), 1, fajl);
        SLOG slog;
        strcpy(slog.evidencioniBroj, "99");    
        strcpy(slog.sifra, "99");    
        strcpy(slog.datum_vreme, "99");    
        strcpy(slog.oznaka, "99");  
        fwrite(&slog, sizeof(SLOG), 1, fajl);  
        

    }
    //
    fclose(fajl);
    
	if (ferror(fajl)) {
		printf("Greska pri upisu u fajl!\n");
	} else {
		printf("Upis novog sloga zavrsen.\n");
	}
    
}


void ispisiSlog(SLOG * slog) {
	printf("%s %s %s %s",
        slog->evidencioniBroj,
		slog->sifra,
		slog->datum_vreme,
		slog->oznaka);
}


void ispisiSveSlogove() {
    FILE* fajl = fopen(aktivna, "rb+");
	if (fajl == NULL) {
		printf("Datoteka nije otvorena!\n");
		return;
	}

	fseek(fajl, 0, SEEK_SET);
	BLOK blok;
	int rbBloka = 0;
	printf("BL SL Evid.Br   Sif.Zat      Dat.Vrem.Dol  Celija  Kazna\n");
	while (fread(&blok, sizeof(BLOK), 1, fajl)) {
        //printf("*");
		for (int i = 0; i < 5; i++) {
            //printf("#");
			if (strcmp(blok.bolesnik[i].evidencioniBroj, OZNAKA_KRAJA_DATOTEKE) == 0) {
				printf("B%d S%d *\n", rbBloka, i);
                break; //citaj sledeci blok
			}
            printf("B%d S%d ", rbBloka, i);
            ispisiSlog(&blok.bolesnik[i]);
            printf("\n");
		}

		rbBloka++;
	}
    fclose(fajl);
}

/*
void citajSekvencijalunDatoteku()
{
    FILE* f = fopen(aktivna, "wb");

    if (f == NULL) {
		printf("Datoteka nije otvorena!\n");
		return;
	}

	fseek(f, 0, SEEK_SET);
	BLOK blok;
	int rbBloka = 0;
	printf("BL SL Evid.Br   Sif.Zat      Dat.Vrem.Dol  Celija  Kazna\n");
	while (fread(&blok, sizeof(BLOK), 1, f)) {

        printf("B%d S%d *\n", rbBloka, 0);
        printf("B%d S%d ", rbBloka, 0);
        printf("%8s", blok.bolesnik.evidencioniBroj);  
        printf("\n");
		
		rbBloka++;
	}
    fclose(f);
}

void formirajSekvencijanuDatoteku(BLOK * serijska)
{
    printf("zzz");
    FILE* f = fopen(aktivna, "wb");
    printf("zzz");
    BLOK* temp = serijska;
    printf("zzz");
    while(temp)
    {
        printf("%s", temp->bolesnik.evidencioniBroj);
        fwrite(&temp->bolesnik.evidencioniBroj, sizeof(char), 8, f);

        temp = temp->sledeci;
    }
    fclose(f);
    //citajSekvencijalunDatoteku(serijska);
}


*/
//4
void formirajSerijsku()
{
    sleep(1);
    int tmpInt;
    char tmpChar[3];
    
    do{
        printf("\nDa li hoces da unses novi slog?");
        printf("\n 0 - Ne ");
        printf("\n 1 - Da ");
        scanf("%d", &tmpInt);
        
        if(tmpInt == 0)
        {
            break;
        }
        if(tmpInt != 1)
        {
            continue;
        }

        SLOG slog;


        printf("\nUnesi evidencioni broj      ");
        scanf("%s", slog.evidencioniBroj);
        slog.evidencioniBroj[2] = '\0'; 

        printf("\nUnesi sifru                 ");
        scanf("%s", tmpChar);
        //printf("%ld",strlen(tmpChar));
        /*
        if(!((strlen(tmpChar)==3))) // ISPRAVITI NA 8
        {
            printf("Pogresana sifra");
            //free(slog);
            sleep(1);
            continue;
        }*/
        strcpy(slog.sifra, tmpChar);
        slog.sifra[2] = '\0'; 
        printf("\nUnesi datum                  ");// DDMMYYYY
        scanf("%s", tmpChar);
        /*
        if(!((strlen(tmpChar)==3))) // ISPRAVITI NA 8
        {
            printf("Pogresana datum");
            sleep(1);
            continue;
        }*/
        strcpy(slog.datum_vreme, tmpChar);
        slog.datum_vreme[2] = '\0'; 
        printf("\nUnesi oznaku                 ");
        scanf("%s", tmpChar);
        /*
        if(!((strlen(tmpChar) == 3))) // ISPRAVITI NA 8
        {
            printf("Pogresana oznaka");
            //free(slog);
            continue;
        }*/
        
        strcpy(slog.oznaka, tmpChar);
        slog.oznaka[2] = '\0'; 
        /*
        printf("\nUnesi duzinu boravka         ");
        scanf("%d", &slog.evidencioniBroj);
        if(!((slog.evidencioniBroj > -1 && (slog.evidencioniBroj < 367)))) // ISPRAVITI NA 8
        {
            printf("Pogresan duzina");
            sleep(1);
            continue;
        }*/
        //slog.evidencioniBroj = (long)tmpInt;
        sleep(1);
        dodajSlog(&slog);

    }while(1);
    //formirajSekvencijanuDatoteku();

}
    

#endif