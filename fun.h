#ifndef FUN_H_INCLUDED
#define FUN_H_INCLUDED

#include "def.h"
#include "fun4.h"

#include <unistd.h>
#include <string.h>



//1
void formirajDatoteku()
{
    FILE *f;
    char name[30];
    printf("Enter the name:");
    scanf("%s",name);

    if((f = fopen(name, "wb"))==NULL)
    {
        printf("Greska");
    }
    BLOK blok;
    strcpy(blok.bolesnik[0].evidencioniBroj, "99");
    fwrite(&blok, sizeof(BLOK), 1, f);

    fclose(f);
}

//2
void odabirAktivne()
{

    //FILE *f;
    char name[30];
    printf("Chose the active one:");
    scanf("%s", name);

    if(access(name, F_OK) != -1 ) {
        printf("Postoji %s\n", name);
        strcpy(aktivna,name);
        sleep(1);
    } else {
        printf("Ne Postoji\n");
        strcpy(aktivna, "");
        sleep(1);
    }
}

//3
void prikazAktivne()
{
    if(aktivna == NULL)
    {
        printf("Nema aktivne");
        sleep(1);
    }
    printf("Aktivna : %s\n", aktivna);
    sleep(1);
}


#endif