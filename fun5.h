#ifndef FUN5_H_INCLUDED
#define FUN5_H_INCLUDED


#include "def.h"

#include <unistd.h>
#include <string.h>



void dodajSlog2(SLOG * slog) {

    FILE* fajl = fopen("sek", "rb+");
	if (fajl == NULL) {
		printf("Datoteka nije otvorena!\n");
		return;
	}
    
    BLOK blok;
    // Provera da li je prvi prazan blok
    if(fread(&blok, sizeof(BLOK), 1, fajl))
    {
        printf("Nije prazna bila");
        BLOK blok;
        fseek(fajl, -sizeof(BLOK), SEEK_END); //u poslednji blok upisujemo novi slog
        fread(&blok, sizeof(BLOK), 1, fajl);

        int i;
        for (i = 0; i < 5; i++) {
            if (strcmp(blok.bolesnik[i].evidencioniBroj, OZNAKA_KRAJA_DATOTEKE) == 0) {
                //Ovo je mesto gde se nalazi slog sa oznakom
                //kraja datoteke; tu treba upisati novi slog.
                memcpy(&blok.bolesnik[i], slog, sizeof(SLOG));
                break;
            }
        }

        i++; //(na to mesto u bloku cemo upisati krajDatoteke)

        if (i < 5) {
            //Jos uvek ima mesta u ovom bloku, mozemo tu smestiti slog
            //sa oznakom kraja datoteke.
            strcpy(blok.bolesnik[i].evidencioniBroj, OZNAKA_KRAJA_DATOTEKE);

            //Sada blok mozemo vratiti u datoteku.
            fseek(fajl, -sizeof(BLOK), SEEK_CUR);
            fwrite(&blok, sizeof(BLOK), 1, fajl);
        } else {
            //Nema vise mesta u tom bloku, tako da moramo
            //praviti novi blok u koji cemo upisati slog
            //sa oznakom kraja datoteke.

            //Prvo ipak moramo vratiti u datoteku blok
            //koji smo upravo popunili:
            fseek(fajl, -sizeof(BLOK), SEEK_CUR);
            fwrite(&blok, sizeof(BLOK), 1, fajl);

            //Okej, sad pravimo novi blok:
            BLOK noviBlok;
            strcpy(noviBlok.bolesnik[0].evidencioniBroj, OZNAKA_KRAJA_DATOTEKE);

            //noviBlok.bolesnik[0].evidencioniBroj = OZNAKA_KRAJA_DATOTEKE;
            //(vec smo na kraju datoteke, nema potrebe za fseekom)
            fwrite(&noviBlok, sizeof(BLOK), 1, fajl);
        }
    }else
    {
        printf("Prazna bila");
        fseek(fajl, 0, SEEK_SET);
        fwrite(&slog, sizeof(SLOG), 1, fajl);
        SLOG slog;
        strcpy(slog.evidencioniBroj, "99");    
        strcpy(slog.sifra, "99");    
        strcpy(slog.datum_vreme, "99");    
        strcpy(slog.oznaka, "99");  
        fwrite(&slog, sizeof(SLOG), 1, fajl);  
        

    }
    //
    fclose(fajl);
    
	if (ferror(fajl)) {
		printf("Greska pri upisu u fajl!\n");
	} else {
		printf("Upis novog sloga zavrsen.\n");
	}
    
}



void formirajSekvencijalnu()
{
    printf("*");
    FILE *fajl;
    if((fajl = fopen(aktivna, "rb"))==NULL)
    {
        printf("Greska");
    }
    printf("*");
    struct SLOG * tmp;
	tmp = (struct SLOG *) malloc(sizeof(struct SLOG));
    //memcpy(&tmp, slog, sizeof(SLOG));
    printf("*");
    
	struct BLOK blok;
	fseek(fajl, 0, SEEK_SET);

    struct SEKV *sekv = NULL;
    struct SEKV  *rep = NULL;
    struct SEKV *rep2 = NULL;
    //sekv = (struct SEKV *) malloc(sizeof(struct SEKV));

    //
    //rep = sekv ;
    //rep->sledeci = NULL;
    printf("*");
    
    while (fread(&blok, sizeof(struct BLOK), 1, fajl)) {

		for (int i = 0; i < 5; i++) {

			if (strcmp(blok.bolesnik[i].evidencioniBroj, "99") == 0) {
                break;
            }   
            //printf(blok.bolesnik[i].evidencioniBroj);
            rep2 = (struct SEKV *) malloc(sizeof(struct SEKV));
            //rep->slog = (SLOG *) malloc(sizeof(SLOG));
            //strcpy(tmp->evidencioniBroj, blok.bolesnik[i].evidencioniBroj);
            //rep->slog = *tmp;
            strcpy(rep2->slog.evidencioniBroj, blok.bolesnik[i].evidencioniBroj);
            //rep->slog.evidencioniBroj[0] = blok.bolesnik[i].evidencioniBroj[0];
            //printf("* %s",rep2->slog.evidencioniBroj);
            
            
            // STARO
            /*
            rep->sledeci = rep2;
            rep = rep->sledeci;
            rep->sledeci = NULL;
            */

            // NOVO

            rep2->sledeci = rep;
            rep = rep2;
        }
    }
    //rep = (SEKV *) malloc(sizeof(SEKV));

    //rep->sledeci = NULL;

    
    //struct SEKV *current = sekv; 
    //printf(" I%sI ", ((((((rep->sledeci)->sledeci)->sledeci)->sledeci)->sledeci)->sledeci)->slog.evidencioniBroj);
    
    //current = current->sledeci;
    struct SEKV *rep3 = rep;
    int length = 0;
    while(rep3 != NULL)
    {
        length++;
        printf("\n + I%sI ", rep3->slog.evidencioniBroj);
        rep3 = rep3->sledeci;
    }


    // ####################
    //        SORT

    int i, j, k, tempKey;
    char tempData[3];
    struct SEKV *current;
    struct SEKV *next;
        
    int size = length;
    k = size ;
        
    for ( i = 0 ; i < size - 1 ; i++, k-- ) {
        current = rep;
        next = rep->sledeci;
            
        for ( j = 1 ; j < k ; j++ ) {   

            if ( strcmp(current->slog.evidencioniBroj, next->slog.evidencioniBroj)>0 ) {
                strcpy(tempData, current->slog.evidencioniBroj);
                strcpy(current->slog.evidencioniBroj ,next->slog.evidencioniBroj);
                strcpy(next->slog.evidencioniBroj ,tempData);
            }
                
            current = current->sledeci;
            next = next->sledeci;
        }
    }   

    // ####################

    // ####################
    // ####################
    //      dodaj u datoteku

    fclose(fajl);

/*
    FILE *fajl2;
    if((fajl2 = fopen("aktivna2", "Wb"))==NULL)
    {
        printf("Greska");
    }*/


    struct SEKV *rep4 = rep;
    //int length = 0;
    while(rep4 != NULL)
    {
        //length++;
        printf("\n / I%sI ", rep4->slog.evidencioniBroj);
        
        strcpy(rep4->slog.sifra , "55");
        strcpy(rep4->slog.datum_vreme , "55");
        strcpy(rep4->slog.oznaka , "55");
        dodajSlog2(&rep4->slog);

        rep4 = rep4->sledeci;
    }

    //rename("sek", aktivna);
    //struct BLOK blok2;
    //strcpy(blok2.bolesnik[0].evidencioniBroj, "99");
    //fwrite(&blok2, sizeof(struct BLOK), 1, fajl2);

    //fclose(fajl2);
    // ####################

    

    
}



#endif