#ifndef FUN6_H_INCLUDED
#define FUN6_H_INCLUDED


#include "def.h"

#include <unistd.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_CHAR 30
#define F_BL 5
#define RST 2
#define MAX_PREKORACILACA 10


// ZONA INDEXA

typedef struct {
    int kljuc;
    int blok;
}SZI;

typedef struct {
    SZI slogovi[2];
    struct BZI* levi;
    struct BZI* desni;
}BZI;



// PRIMARNA ZONA

typedef struct {
    struct SLOG slogovi[5];
    struct BPZ *glava;
}BPZ;

// ZONA PREKORACILACA

typedef struct  {
    struct SLOG prekoslog;
    struct SPR  *sledeci;
}SPR;



/*
typedef struct {
    int brBlokovaPz; //broj blokova primarne zone
    int brBlokovaZi; //broj blokova zone indeksa
    int pocetkaZi;
    int koren;
} ZAG;


typedef struct {
    BZI blok;
    int kljuc;
}ELbrojCvorova;              //ElbrojCvorovaentNiza
*/

int numberOfBlocks()
{

    FILE *sekv;
    int length = 0;
    sekv = fopen(aktivna,"rb");
    struct BLOK tbrojCvorovap;

    while(fread(&tbrojCvorovap,sizeof(struct BLOK),1,sekv))
    {
        length++;
    }
    fclose(sekv);
    return length;
}


void formirajIndexSekvencijalnu()
{
    int length = numberOfBlocks();
    int depth = log2(length)+1;
    int cvorovi = pow(2,depth)-1;
    int listovi = pow(2,depth-1);

    printf("%d",length);
    printf("\n%d",depth);
    printf("\n%d",cvorovi);
    printf("\n%d",listovi);

    //FILE *sekv = fopen("sek","rb");

    FILE *sekv = fopen(aktivna,"rb");
    int i = 0;

    BLOK blok;
    BLOK PrimarnaZona[length];

     printf("\n\n\n\n");
    while(fread(&blok, sizeof(struct BLOK), 1, sekv))
    {
        printf("%d ", atoi(blok.bolesnik[0].evidencioniBroj));
        PrimarnaZona[i] = blok;
        //printf("%d ", atoi(PrimarnaZona[i].bolesnik[0].evidencioniBroj));

        i++;
    }

    fclose(sekv);

    printf("\n\n\n\n");
    SZI Nivolistova[listovi];
    for(int j = 0; j< listovi; j++)
    {
        Nivolistova[j].blok = (j/2)*5;
        Nivolistova[j].kljuc = 99;

        for(int k = 0; k < 5; k++)
        {
            printf("%d \n", atoi(PrimarnaZona[j].bolesnik[k].evidencioniBroj));
            Nivolistova[j].kljuc = atoi(PrimarnaZona[j].bolesnik[k].evidencioniBroj);
            if(strcmp(PrimarnaZona[j].bolesnik[k].evidencioniBroj, "99")==0)
            {
                break;
            }
        }
        printf("%d \n", Nivolistova[j].kljuc);
    }
    printf("\n\n\n\n");
    // Dinamicki listovi

    BZI listi[(listovi+1)/2];
    for(int j = 0; j < (listovi+1)/2; j++)
    {
        listi[j].slogovi[0] = Nivolistova[j*2];
        listi[j].slogovi[1] = Nivolistova[j*2 + 1];  
        listi[j].desni = NULL;
        listi[j].levi = NULL;  
    }

    for(int d = 0; d < depth; d++){
        for(int t = 0; t < (listovi+1)/2; t++)
        {
            printf("%d ", listi[t].slogovi[0].kljuc);
            printf("%d ", listi[t].slogovi[1].kljuc);
        }
    }
    /*
    for(int m = 0; m < depth; m++)
    {

    }*/
}
/*
    int i = -1;
    while((fread(&blok, sizeof(BLOK),1,sekv)!=NULL) && (strcmp(blok.evidencioni_broj, "99")!=0))
    {
        i++;
        if (i%2==0){
            // Alocira se
            s1 = *((SZI*) malloc(sizeof(SZI)));
            // Popunjava se
            s1.kljuc = blok.bolesnik[4].evidencioniBroj;
            //s1.adresa_bloka = sizeof(Zaglavlje) + i*sizeof(Blok_primarne_zone) + sizeof(Blok_primarne_zone);
        }
        else{
            // Alocira se
            s2 = *((SZI*) malloc(sizeof(SZI)));
            // Popunjava se
            s2.kljuc = blok.bolesnik[4].evidencioniBroj;
            //s2.adresa_bloka=sizeof(Zaglavlje) + i*sizeof(Blok_primarne_zone) + sizeof(Blok_primarne_zone);
        }

        if (i%2 == 1){
            bzi.slogovi[0] = s1;
            bzi.slogovi[1] = s2;
            //dodajList(&glava,&kBlo);
            niz[brojCvorova].blok = bzi;
            niz[brojCvorova].kljuc = brojCvorova;
            brojCvorova++;
        }
    }

    if (length % 2 == 1){
        bzi.slogovi[0] = s1;
        s2 = *((SZI*) malloc(sizeof(SZI)));
        s2.adresa_bloka = -1;
        s2.kljuc = 0;
        bzi.slogovi[1] = s2;
        //dodajList(&glava,&kBlo);
        niz[brojCvorova].blok = bzi;
        niz[brojCvorova].kljuc = brojCvorova;
        brojCvorova++;
    }
    fclose(sekv);

    for(i = brojCvorova; i<length; i++){
        niz[i].blok = bzi;
        niz[i].kljuc = i;
    }

    int j;
    printf("\nClanovi niza : ");
    for(j=0;j<length;j++)
        printf("\nprvi slog [%ld,%d],drugi slog [%ld,%d]",niz[j].blok.slogovi[0].kljuc,niz[j].blok.slogovi[0].adresa_bloka,niz[j].blok.slogovi[1].kljuc,niz[j].blok.slogovi[1].adresa_bloka);

    //StabloTrazenja *stablo;
    //formirajStablo(niz,brCvorova,&stabloTrazenja);

    //prikaziStablo(stabloTrazenja);
}
*/
#endif