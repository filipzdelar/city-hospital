#ifndef DEF_H_INCLUDED
#define DEF_H_INCLUDED

#define OZNAKA_KRAJA_DATOTEKE "99"

typedef struct SLOG{
    char evidencioniBroj[3]; // == 8 
    char sifra[3];       // 7
    char datum_vreme[3]; // == 8 DDMMYYYY
    char oznaka[3];      // == 5
    //int duzinaBoravka;   // <3
}SLOG;

typedef struct BLOK{
    struct SLOG bolesnik[5];
    struct BLOK* sledeci;
}BLOK;

typedef struct SEKV{
    // Ispraviti ako stignes do BLOK-a
    struct SLOG slog;
    struct SEKV* sledeci;
}SEKV;

typedef struct CVOR{
    //ElementNiza blokZI;
    struct CVOR *levo;
    struct CVOR *desno;
    int left ;
    int right ;
} CVOR;

typedef struct STABLO {
    struct CVOR *pok;
} STABLO;

#endif